package aplicacion;
import dominio.* ;

public class Principal {
	public static void main (String[] args) {
		Localidad localidad1 = new Localidad();
		localidad1.setNombre("Tarifa");
		localidad1.setNumeroDeHabitantes(20000);
		
		Localidad localidad2 = new Localidad();
		localidad2.setNombre("Algeciras");
		localidad2.setNumeroDeHabitantes(200000);
	
		Localidad localidad3 = new Localidad();
		localidad3.setNombre("Facinas");
		localidad3.setNumeroDeHabitantes(3000);
		
		Localidad localidad4 = new Localidad();
		localidad4.setNombre("Tahivilla");
		localidad4.setNumeroDeHabitantes(500);

		Municipio municipio1 = new Municipio();
		municipio1.setNombre("Campo de Gibraltar");
		municipio1.annadirLocalidad(localidad1);
		municipio1.annadirLocalidad(localidad2);
		System.out.println(municipio1.contarNumeroHabitantes());
		System.out.println(municipio1);

		Municipio municipio2 = new Municipio();
		municipio2.setNombre("FT");
		municipio2.annadirLocalidad(localidad3);
		municipio2.annadirLocalidad(localidad4);
		System.out.println(municipio2.contarNumeroHabitantes());
		System.out.println(municipio2);
		
		Provincia provincia1 = new Provincia();
		provincia1.setNombre("Cádiz");
		provincia1.annadirMunicipio(municipio1);
		provincia1.annadirMunicipio(municipio2);
		System.out.println(provincia1.contarNumeroHabitantes());
		System.out.println(provincia1);

	}
}
