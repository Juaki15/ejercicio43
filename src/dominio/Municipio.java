package dominio;
import java.util.ArrayList;

public class Municipio {
	
	private String nombre;
	ArrayList <Localidad> localidades = new ArrayList<>();
	private int numeroTotalHabitantes;
	
	public int contarNumeroHabitantes(){
		for (Localidad l : localidades){
			numeroTotalHabitantes += l.getNumeroDeHabitantes();
			System.out.println("El numero de habitantes de " + l.getNombre() + " es " + l.getNumeroDeHabitantes());
		}
		return  numeroTotalHabitantes;
	}
	public ArrayList annadirLocalidad(Localidad l){
		localidades.add(l);
		return this.localidades;
	}
	public String getNombre () {
		return nombre;
        }
	public void setNombre(String nombre){
                this.nombre = nombre;
        }
	public int getNumeroTotalHabitantes(){
		return this.numeroTotalHabitantes;
	}

	public String toString(){
	        return "Nombre: " + nombre + "\nNumero de habitantes: " + numeroTotalHabitantes;
	}
}
