package dominio;
import java.util.ArrayList;

public class Provincia{
	private String nombre;
        ArrayList <Municipio> municipios = new ArrayList<>();
        private int numeroTotalHabitantes;
        public int contarNumeroHabitantes(){
		for (Municipio m : municipios){
			numeroTotalHabitantes += m.getNumeroTotalHabitantes();
        	        System.out.println("El numero de habitantes de " + m.getNombre() + " es " + m.getNumeroTotalHabitantes());
		}
		return  numeroTotalHabitantes;
        }
        public ArrayList annadirMunicipio(Municipio m){
		municipios.add(m);
	        return this.municipios;
	}
	public String getNombre () {
               return nombre;
        }
        public void setNombre(String nombre){
                this.nombre = nombre;
        }
	public int getNumeroTotalHabitantes(){
		return this.numeroTotalHabitantes;
	}

        public String toString(){
                return "Nombre: " + nombre + "\nNumero de habitantes: " + numeroTotalHabitantes;
        }	
}
